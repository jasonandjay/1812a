import {Layout} from 'antd'
import { useLocation } from 'umi';
import MyHeader from '@/components/MyHeader';
import MyMenu from '@/components/MyMenu';

const whiteList = ['/login', '/register', '/article/editor', '/article/amEditor']
const DefaultLayout: React.FC = (props)=>{
    const location = useLocation();
    if (whiteList.indexOf(location.pathname) !== -1){
        return <>{props.children}</>;
    }
    return <Layout>
        {/* <MyMenu></MyMenu> */}
        <Layout>
            <MyHeader></MyHeader>
            {/* 面包屑 */}
            {props.children}
        </Layout>
    </Layout>
}

export default DefaultLayout;