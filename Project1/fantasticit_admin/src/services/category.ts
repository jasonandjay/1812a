import { request } from 'umi';

// 获取分类列表
export function getCategoryList(){
    return request('/api/category');
}