import { ILoginForm } from '@/type';
import { request } from 'umi';

// 登陆接口
export function login(data: ILoginForm){
    return request('/api/auth/login', {
        method: 'POST',
        data
    })
}