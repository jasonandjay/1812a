import { IArticleItem } from '@/type/article';
import { request } from 'umi';

// 发布文章
export function publishArticle(data: Partial<IArticleItem>){
    return request('/api/article', {
        method: 'POST',
        data
    });
}