import { IMailQuery, IReplyItem } from '@/type';
import { request } from 'umi';

// 获取邮件接口
export function getMailList(page=1, params:Partial<IMailQuery>={}, pageSize=12){
    return request(`/api/smtp?page=${page}&pageSize=${pageSize}`, {params})
}

// 删除邮件
export function deleteMail(id: string){
    return request(`/api/smtp/${id}`, {
        method: 'DELETE'
    })
}

// 回复邮件
export function replyMail(data: IReplyItem){
    return request('http://127.0.0.1:7001/mail', {
        method: 'POST',
        data
    })
}