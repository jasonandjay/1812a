import useStore from '@/context/useStore';
import { ILoginForm } from '@/type';
import {Form, Input, Button} from 'antd'
import {Link, useHistory, useLocation} from 'umi'
import {observer} from 'mobx-react-lite'

interface ILocationQuery{
    query: {
        from: string;
    }
}
const Login = ()=>{
    const store = useStore();
    const history = useHistory();
    const location = useLocation();

    console.log('store...', store, location);
    async function submit(values: ILoginForm){
        let result = await store.user.login(values)
        if (Boolean(result)){
            let redirect = '/';
            if ((location as unknown as ILocationQuery).query.from){
                redirect = decodeURIComponent((location as unknown as ILocationQuery).query.from);
            }
            history.replace(redirect);
        }
    }

    return <div>
        <h2>系统登陆</h2>
        <div>{JSON.stringify(store.user.userInfo)}</div>
        <p>{store.user.userInfo.name}</p>
        <Form
            onFinish={submit}
            initialValues={{name:'admin',password:'admin'}}
        >
            <Form.Item
                name="name"
                label="账号"
                rules={[
                    {required:true, message:'请输入用户名'},
                    {pattern:/\w{5,12}/, message:'请输入合法的用户名'}
                ]}
            >
                <Input type="text" placeholder="请输入用户名"/>   
            </Form.Item> 
            <Form.Item
                name="password"
                label="密码"
                rules={[{required:true, message:'请输入密码'}]}
            >
                <Input type="password" placeholder="请输入密码"/>   
            </Form.Item> 
            <Button htmlType="submit">登陆</Button>
            <p>
                Or
                <Link to="/register">注册用户</Link>
            </p>
        </Form>
    </div>
}

export default observer(Login);