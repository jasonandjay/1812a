import { useHistory } from 'umi';
import styles from './index.less';

export default function IndexPage() {
  const history = useHistory();
  
  return (
    <div>
      <h1 className={styles.title}>Page index</h1>
      <button onClick={()=>history.replace('/login')}>登陆</button>
    </div>
  );
}
