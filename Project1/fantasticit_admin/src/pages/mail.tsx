import useStore from '@/context/useStore';
import { replyMail } from '@/services';
import { IMailItem } from '@/type';
import { makeHtml } from '@/utils/markdown';
import { Form, Button, Input, Table, Modal, message } from 'antd'
import {observer} from 'mobx-react-lite'
import { Key, useEffect, useState } from 'react';


const Main = ()=>{
    const [form] = Form.useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [params, setParams] = useState({});
    const [reply, setReply] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [current, setCurrent] = useState<IMailItem>();
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key []>([])

    // 获取邮件列表
    useEffect(() => {
        store.mail.getMailList(page, params);
    }, [page, params]);

    // 条件查询
    function submit(){
        let values = form.getFieldsValue();
        let params:{[key: string]: string} = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
        setPage(1);
    }
    // 表格选中操作
    function onSelectChange(selectedRowKeys: Key[], selectedRows: IMailItem[]){
        setSelectedRowKeys(selectedRowKeys);
    }
    // 回复内容
    async function replayMail(){
        if (!reply){
            message.warn('请输入回复内容');
            return;
        }
        let result = await replyMail({
            to: current?.to!,
            subject: `回复${current?.from}`,
            html: makeHtml(reply)
        })     
        console.log('result...', result);
        setShowModal(false);
        if(result.message === '发送邮件成功'){
            message.success(result.message);
        }else{
            message.error(result.message);
        }
    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }

    const columns = [{
        title: '发件人',
        dataIndex: 'from'
    }, {
        title: '收件人',
        dataIndex: 'to'
    }, {
        title: '主题',
        dataIndex: 'subject'
    }, {
        title: '发送时间',
        dataIndex: 'createAt'
    }, {
        title: '操作',
        render: (item: IMailItem)=>{
            return <div>
                <Button onClick={()=>store.mail.deleteMail([item.id])}>删除</Button>
                <Button onClick={()=>{
                    setShowModal(true);
                    setCurrent(item);
                }}>回复</Button>
            </div>
        }
    }]
    return <div>
        <Form
            form={form}
            onFinish={submit}
        >
            <Form.Item
                label="发件人"
                name="from"
            >
                <Input type="text" placeholder="请输入发件人"/>
            </Form.Item>
            <Form.Item
                label="收件人"
                name="to"
            >
                <Input type="text" placeholder="请输入收件人"/>
            </Form.Item>
            <Form.Item
                label="主题"
                name="subject"
            >
                <Input type="text" placeholder="请输入主题"/>
            </Form.Item>
            <Button htmlType="submit" type="primary">搜索</Button>
            <Button htmlType="reset">重置</Button>
        </Form>
        <p>
            {selectedRowKeys.length?<Button onClick={()=>store.mail.deleteMail(selectedRowKeys as string[])}>删除</Button>:null}
            <Button onClick={()=>{
                setPage(1);
                setParams({...params});
            }}>刷新</Button>
        </p>
        <Table 
            rowSelection={rowSelection}
            // loading={Boolean(store.mail.mailList.length)}
            columns={columns} 
            dataSource={store.mail.mailList} 
            rowKey="id" 
            pagination={{showSizeChanger:true}}
        />
        <Modal 
            title={`回复${current?.to}`}
            onCancel={()=>setShowModal(false)}
            visible={showModal}
            footer={<Button onClick={()=>replayMail()}>回复</Button>}
        >
            <Input.TextArea placeholder="回复内容支持md,html和普通文本" rows={8} value={reply} onChange={e=>setReply(e.target.value)}></Input.TextArea>
        </Modal>
    </div>
}

export default observer(Main);