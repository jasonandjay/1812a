import { getCategoryList } from "@/services";
import { ICategoryItem } from "@/type";
import { makeAutoObservable, runInAction } from "mobx"

class Category{
    categoryList: ICategoryItem[] = [];
    constructor(){
        makeAutoObservable(this);
    }

    async getCategoryList(){
        let result = await getCategoryList()
        if (result.data){
            runInAction(()=>{
                this.categoryList = result.data;
            })
        }
    }
}

export default Category;