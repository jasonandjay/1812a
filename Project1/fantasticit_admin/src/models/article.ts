import { publishArticle } from "@/services";
import { IArticleItem, ICategoryItem } from "@/type";
import { makeAutoObservable, runInAction } from "mobx"

class Article{
    constructor(){
        makeAutoObservable(this);
    }

    // 发布文章
    async publishArticle(data: Partial<IArticleItem>){
        let result = await publishArticle(data);
        return result.data;
    }
}

export default Article;