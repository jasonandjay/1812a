// 引入状态管理模块
import User from './user'
import Mail from './mail'
import Category from './category'
import Tag from './tag'
import Article from './article'

export default {
    user: new User,
    mail: new Mail,
    category: new Category,
    tag: new Tag,
    article: new Article
}