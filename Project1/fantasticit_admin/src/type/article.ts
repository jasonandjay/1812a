export interface IArticleItem {
  title: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  tags: any[];
  cover?: string;
  password?: string;
  totalAmount?: string;
  id: string;
  views: number;
  likes: number;
  isPay: boolean;
  createAt: string;
  updateAt: string;
}