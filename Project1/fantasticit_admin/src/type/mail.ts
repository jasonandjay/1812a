export interface IMailItem {
  id: string;
  from: string;
  to: string;
  subject: string;
  text?: any;
  html: string;
  createAt: string;
}
export interface IMailQuery {
  from: string;
  to: string;
  subject: string;
}

export interface IReplyItem{
  to: string;
  subject: string;
  html: string;
}