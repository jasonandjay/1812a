export interface ILoginForm{
    name: string;
    password: string;
}
export interface IUserInfo {
  id: string;
  name: string;
  avatar?: any;
  email?: any;
  role: string;
  status: string;
  createAt: string;
  updateAt: string;
  token: string;
}