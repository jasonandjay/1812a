import {Layout} from 'antd'

const {Header} = Layout;
const MyHeader: React.FC = ()=>{
    return <Header>自定义头部字段</Header>
}

export default MyHeader;