'use strict';
// const AlipaySdk = require('alipay-sdk');
// const AlipayFormData = require('alipay-sdk/lib/form');
const Controller = require('egg').Controller;

// 普通公钥模式
// const alipaySdk = new AlipaySdk({
//     // 参考下方 SDK 配置
//     appId: '2021000118615202',
//     privateKey: `MIIEpAIBAAKCAQEAghG3dpihvVotvYCUYz4aCju3SwqgrAsWcKC+T9tONkxwticrwhQpEOB4Fhbug2Ltq+ubcLTE4h5VxOZQsba992msXB2EgVE4OYpO4GoNA9wEdNdvkte7rBe6TQ0Vq4iOiau8GV2jQCHbDXwd1fGHJrRys9KauuBH4pdCu33fwYx3WF8yB41WCRNVmCx3tr5P2vT2CIV88pdCYkeFDUhRp6A47Sz76zd9O/w7Pwq/9u6v1r259BoBYRvyhv9jGfj0TX5w1WizKczGiHRafnfLndDz8qvYYsQpuADJuqvxCSJ0V/MwRb++x/KHJu2zV1BOziwhck5W+sXNYDf4QGh2rwIDAQABAoIBABJ1mzj8nm+2Ham4Vtbi2GU9Zl0DDBx+kle3qGrC8A2quZBQbNhlbhKUGNbhuEXzQ1HT5UKQlSSWM4v3N9qQ2dMEL07/bW/A7B7Wby1QP7qdfhBrBs2rSlo5H9qRkdK/VaNaOR7MeD/AzkrSo5VaatjmYST+LbF55RnWZ8UeLyRTrBvhwwcOWTw2a5fnPp3Bg8M+Eage6dMKN0ORSwyOXDLJI6aTZ3oYBsiQo+eNvUJjKrnIKn/muk6Jvve5R1XAaKfBau7s3KqwT8U1JPmvJDku1DZABJdhpDff+6q18aTQWgg3EtlVmc+tKDot0zTcr70Lj8AauKie35OIBSriUoECgYEAxtqxPbMiqEssy4izOBu3HB+xlByWRJOLrkXIoYFkresWWGP/yCfZFRSW90oAd4UtBglcGJVykGsAoZD3Vvn6htGRurJWO0hBZBo+ZgnImYgkNop4/3djn9FiR6VPNCwB/wXVrLMn6UtiR2RDm0OEEBT9ClXPuzUuFQ19qGalPcECgYEAp3KlXpO9KOxl3sw1qbXVNFDxZDp412DVSwOEIQLySgElfD8bsg6F0xFreRKxd+ttkxiwlrdUc25igbh0IdcsMFJHaU7sYuuQU3+NnRJywXKmLaW8XHkJTIIfsMW/Cx720Z7SURwsPSlcCfEflUPGZLa53xv8dFIWb0qCXTPOsG8CgYEAvvyL/pZuUvTgsRs6BkmYNIq8I2TRbkvHfDnJ2FROPjvr2jkxESw0cuqzlA6fJCvkckfLDFF9FveuQ37YD9FHrPTa4lUmPQnIxDvwclAzOkJnhXq5e6NPK9AOdIFw8MrTn/wanSP00Tx1x9ukkxrlXAzMe4CvYz5QZCfw6CytyAECgYBEEryd9WvaPjiv2c8bHlv9t8Za29aSoMA1jclibkM3yL2HhFRrD7Jh+1v4zZuuR9y1Bz2LGB7SCpcra2Ry+XnybxCvBDHfiSqUd2+H4p8ae9SJDC7GXUJlgyUFBW9EQyNOVsXjyZNGjhDnzcnsMbBu80YfKM56L4EFX4IeB9jIKwKBgQCllDjb7DdR/VJK4Pz1LlWuKx8p3GBajFQxofx/EZ6oqni9WmU7CkTC7QaHMDM17lCYmfNl9Pe8ysdGVPjlyjrrRru+EuV/oDPSYaGMnp3maccxa7AwZspf2+D2WkxP6I7rY9l7901S0rREIgnWEHInB2SDl4Am4iLy2cX7/xiRsA==`,
//     //可设置AES密钥，调用AES加解密相关接口时需要（可选）
//     encryptKey: 'Vdo+ByKhn8UW5pMuO7t3uA=='
// });

class PayController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }

  // 创建支付
  async create(){
    const { ctx } = this;
    const {totalAmount, id} = ctx.request.body;
    const formData = new AlipayFormData();
    // 调用 setMethod 并传入 get，会返回可以跳转到支付页面的 url
    formData.setMethod('get');

    formData.addField('notifyUrl', 'http://www.com/notify');
    formData.addField('bizContent', {
        outTradeNo: id,
        productCode: 'FAST_INSTANT_TRADE_PAY',
        totalAmount: totalAmount,
        subject: '商品',
        body: '商品详情',
    });

    const result = await alipaySdk.exec(
    'alipay.trade.page.pay',
    {},
    { formData: formData },
    );

    // result 为可以跳转到支付链接的 url
    console.log(result);
    this.ctx.body = {
        url: result
    }
  }
}

module.exports = PayController;
