'use strict';

const fs = require('fs');
const path = require('path');
const Controller = require('egg').Controller;
const COS = require('cos-nodejs-sdk-v5');
const base64js = require('base64-js')
const cos = new COS({
   SecretId: 'AKIDMXS8XfYAVZMs75mDNGsDI6dsqYz92ez3',
   SecretKey: 'oLccLmyua1DAsKXdS0IJ45qLZWDdmfHc'
});
const saveDir = path.resolve(__dirname, '../public');

class FileController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }

  // 列出oss的存储桶
  async listBuckets(){
    const { ctx } = this;

    try{
        let result = await (()=>new Promise((resolve, reject) => {
            cos.getService(function (err, data) {
                if (err){
                    reject(err);
                    ctx.body = err;
                }
                resolve(data);
            });
        }))()
        ctx.body = result;
    }catch(err){
        ctx.body = err;
    }
  }

  // 上传文件到oss
  async putObject(){
    const { ctx } = this;
    const stream = await ctx.getFileStream();
    // console.log('stream...', stream);
    ctx.body = stream;
    try{
        let result = await (()=>new Promise((resolve, reject) => {
            cos.putObject({
                Bucket: 'bw-1253274769', /* 必须 */
                Region: 'ap-beijing',    /* 必须 */
                Key: stream.fieldname,              /* 必须 */
                StorageClass: 'STANDARD',
                Body: stream, // 上传文件对象
                onProgress: function(progressData) {
                    console.log(JSON.stringify(progressData));
                }
             }, function(err, data) {
                console.log(err || data);
                if (err){
                    reject(err);
                }else{
                    resolve(data);
                }
             });
        }))()
        ctx.body = result;
    }catch(err){
        ctx.body = err;
    }
  }

  // 上传图片同时添加盲水印
  async putWaterImage(){
    const { ctx } = this;
    const stream = await ctx.getFileStream();
    // 盲水印还是显示水印
    const show = ctx.getField('show') || false;
    const reg = /.(\w+)$/.exec(stream.fieldname);
    const waterText = 'MTgxMkE=';
    console.log('pic...', `{"is_pic_info": 1, "rules": [{"fileid":"${stream.fieldname.replace(reg[0], `_water${reg[0]}`)}", "rule": "watermark/3/type/3/text/${waterText}"}]}`)
    try{
        let result = await (()=>new Promise((resolve, reject) => {
            cos.putObject({
                Bucket: 'bw-1253274769', /* 必须 */
                Region: 'ap-beijing',    /* 必须 */
                Key: stream.fieldname,              /* 必须 */
                StorageClass: 'STANDARD',
                Body: stream, // 上传文件对象
                Headers: {
                    // 万象持久化接口，上传时持久化
                    'Pic-Operations': `{"is_pic_info": 1, "rules": [{"fileid":"${stream.fieldname.replace(reg[0], `_water${reg[0]}`)}", "rule": "watermark/3/type/3/text/${waterText}"}]}`
                  },
             }, function(err, data) {
                console.log(err || data);
                if (err){
                    reject(err);
                }else{
                    resolve(data);
                }
             });
        }))()
        ctx.body = result;
    }catch(err){
        ctx.body = err;
    }
  }

  // 下载oss文件到本地
  async downloadObject(){
    const { ctx } = this;
    // console.log('stream...', stream);
    const {filename} = ctx.request.body;
    try{
        let result = await (()=>new Promise((resolve, reject) => {
            cos.getObject({
                Bucket: 'bw-1253274769', /* 必须 */
                Region: 'ap-beijing',    /* 必须 */
                Key: filename,              /* 必须 */
                Output: fs.createWriteStream(path.resolve(__dirname, `../public/${filename}`)),
             }, function(err, data) {
                console.log(err || data);
                if (err){
                    reject(err);
                }else{
                    resolve(data);
                }
             });
        }))()
        ctx.body = result;
    }catch(err){
        ctx.body = err;
    }
  }

  // 存储大文件的分片
  async saveChunk(){
    const { ctx } = this;
    const stream = await ctx.getFileStream();
    // console.log('stream...', stream);
    try{
         // 分片存储的文件夹
        const chunkDir = path.join(saveDir, `/${stream.fieldname}`)
        if (!fs.existsSync(chunkDir)) {
            fs.mkdirSync(chunkDir);
        }
        // 文件存储路径
        const chunkFile = fs.createWriteStream(path.join(chunkDir, `/${stream.fieldname}_${stream.fields.index}`));
        stream.pipe(chunkFile);
        ctx.body = {'msg': '分片存储成功'}
    }catch(e){
        console.log('e...', e);
        ctx.body = {'msg': '分片存储失败'}
    }
  }

  // 合并分片文件
  async mergeChunk(){
    const { ctx } = this;
    const {total, chunkname, filename} = ctx.request.body;
    let index = 1;
    try{
        fs.writeFileSync(path.join(saveDir, `/${filename}`), '');
        while(total>=index){
            fs.appendFileSync(path.join(saveDir, `/${filename}`), fs.readFileSync(path.join(saveDir, `/${chunkname}/part_${index}`)));
            index++;
        } 
        ctx.body = {'msg': '分片合并成功'};
    }catch(e){
        console.log('e...', e);
        ctx.body = {'msg': '分片合并失败'};
    }
  }
}

module.exports = FileController;
