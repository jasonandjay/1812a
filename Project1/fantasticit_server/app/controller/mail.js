'use strict';

const Controller = require('egg').Controller;
const nodemailer = require("nodemailer");

class MailController extends Controller {
  async index() {
    const {
      ctx
    } = this;
    ctx.body = 'hi, egg';
  }

  async getTransporter() {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.qq.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: "210001837", // generated ethereal user
        pass: "bbbvwmyjpybdbjgc", // generated ethereal password
      }
    });
    return transporter;
  }

  async create() {
    const {
      ctx
    } = this;
    let {to, subject, html} = this.ctx.request.body;

    try{
      let transporter = await this.getTransporter();

      let info = await transporter.sendMail({
        from: '👻自动发件👻<210001837@qq.com>', // sender address
        to: to || "342690199@qq.com, 210001837@qq.com", // list of receivers
        subject: subject || "Hello ✔", // Subject line
        html: html || "<b>Hello world?</b>", // html body
      });
      console.log('info...', info);
      this.ctx.body = {
        message: '邮件发送成功'
      }
    }catch(e){
      console.log('e...', e);
      this.ctx.body = {
        message: '邮件发送失败'
      }      
    }
  }
}

module.exports = MailController;