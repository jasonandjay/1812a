'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  /**
   * Restful风格
   * Method	Path	Route Name	Controller.Action
    GET	/mail	mail	app.controllers.mail.index
    GET	/mail/new	new_post	app.controllers.mail.new
    GET	/mail/:id	post	app.controllers.mail.show
    GET	/mail/:id/edit	edit_post	app.controllers.mail.edit
    POST	/mail	mail	app.controllers.mail.create
    PUT	/mail/:id	post	app.controllers.mail.update
    DELETE	/mail/:id	post	app.controllers.mail.destroy
   */
  // 配置邮件接口
  router.resources('/mail', controller.mail);

  // 配置支付接口
  // router.resources('/pay', controller.pay);

  // 配置文件接口
  router.resources('/file', controller.file);
  router.get('/file/listBuckets', controller.file.listBuckets);
  router.post('/file/putObject', controller.file.putObject);
  router.post('/file/putWaterImage', controller.file.putWaterImage);
  router.post('/file/downloadObject', controller.file.downloadObject);
  router.post('/file/saveChunk', controller.file.saveChunk);
  router.post('/file/mergeChunk', controller.file.mergeChunk);
};
