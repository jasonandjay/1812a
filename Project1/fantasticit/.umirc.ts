import { defineConfig } from 'umi';
const px2rem = require('postcss-px2rem');

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {},
  dva: {
    immer: true,
    hmr: true,
  },
  locale: {},
  extraPostCSSPlugins: [px2rem({remUnit: 75})],
  // tbcdn不支持https的方式引入，切换成本地加载
  // scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],
  // 配置资源线上打包路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812A/chenmanjie/fantasticit/' : '/',
  // 配置线上路由前缀
  base: process.env.NODE_ENV === 'production' ? '/1812A/chenmanjie/fantasticit' : '/',
  // 配置路由懒加载
  dynamicImport: {
    loading: '@/components/Loading'
  },
  // 配置文件hash后缀，使用增量发布策略
  hash: true,
  // 增加数据埋点，百度统计
  analytics: {
    baidu: 'b7a0296bf19580a2cab8bb41ec0d0d4f'
  },
  // 配置本地环境代码
  proxy: {
    '/front': {
      'target': 'https://api.blog.wipi.tech/',
      'changeOrigin': true,
      'pathRewrite': { '^/front' : '' },
    },
  },
});
