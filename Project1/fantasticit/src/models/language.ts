import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface LanguageModelState {
  locale: string;
  locales: {label: string, value: string}[]
}

export interface LanguageModelType {
  namespace: 'language';
  state: LanguageModelState;
  reducers: {
    save: Reducer<LanguageModelState>;
  };
}

const LanguageModel: LanguageModelType = {
  namespace: 'language',
  state: {
    locale: 'zh-CN',
    locales: [
      {label: 'menu.language.chinese', value: 'zh-CN'},
      {label: 'menu.language.english', value: 'en-US'},
      {label: 'menu.language.japan', value: 'ja-JP'},
      {label: 'menu.language.korea', value: 'ko-KR'}
    ]
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default LanguageModel;