import { getArticleComment, getArticleDetail, getArticleList, getRecommend } from '@/services';
import { IRootState } from '@/types';
import { IArticleComment, IArticleDetail, IArticleItem } from '@/types/article';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface ArticleModelState {
  recommend: IArticleItem[];
  articleList: IArticleItem[];
  articleCount: number;
  articleDetail: Partial<IArticleDetail>;
  articleComment: IArticleComment [];
  articleCommentCount: number; 
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    getArticleDetail: Effect;
    getArticleComment: Effect;
  };
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const IndexModel: ArticleModelType = {
  namespace: 'article',

  state: {
    recommend: [],
    articleList: [],
    articleCount: 0,
    articleDetail: {},
    articleComment: [],
    articleCommentCount: 0 
  },

  effects: {
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        })
      }
    },
    *getArticleList({ payload }, { call, put, select }) {
      let articleList = yield select((state:IRootState)=>state.article.articleList);
      let result = yield call(getArticleList, payload);
      if (result.statusCode === 200){
        articleList = payload===1?result.data[0]: [...articleList, ...result.data[0]]
        yield put({
          type: 'save',
          payload: {
            articleList,
            articleCount: result.data[1]
          }
        })
      }
    },
    *getArticleDetail({ payload }, { call, put }) {
      let result = yield call(getArticleDetail, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { articleDetail: result.data }
        })
      }
    },
    *getArticleComment({ payload }, { call, put, select }) {
      // let articleComment = yield select((state:IRootState)=>state.article.articleList);
      let result = yield call(getArticleComment, payload.id, payload.page);
      if (result.statusCode === 200){
        yield put({
          type: 'save',
          payload: {
            articleComment: result.data[0],
            articleCommentCount: result.data[1]
          }
        })
      }
    },

  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;