import copy from 'copy-to-clipboard';
import {message} from 'antd';

export function copyText(text: string){
    copy(text);
    message.success('copy success!');
}