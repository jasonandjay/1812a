export function fomatTime(time: string){
    let date = new Date(time);
    let years = date.getFullYear(),
        month = String(date.getMonth()+1).padStart(2, '0'),
        day = String(date.getDate()).padStart(2, '0'),
        hour = String(date.getHours()).padStart(2, '0'),
        min = String(date.getMinutes()).padStart(2, '0'),
        sec = String(date.getSeconds()).padStart(2, '0');
    return `${years}-${month}-${day} ${hour}:${min}:${sec}`;
}