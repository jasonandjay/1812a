export default {
    'menu.article': '文章',
    'menu.archive': 'アーカイブ',
    'menu.knowledge': '知識小冊',
    'menu.message': '伝言板',
    'menu.about': 'について',
    'menu.language.chinese': '中国語',
    'menu.language.english': '英語',
    'menu.language.japan': '日本',
    'menu.language.korea': '韓国'
}