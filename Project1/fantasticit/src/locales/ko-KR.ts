export default {
    'menu.article': '문장.',
    'menu.archive': '압축 파일',
    'menu.knowledge': '지식 소책자',
    'menu.message': '메모판',
    'menu.about': '...에 대하 여',
    'menu.language.chinese': '중국어.',
    'menu.language.english': '영어.',
    'menu.language.japan': '日일본.',
    'menu.language.korea': '한국.'
}