import { request } from 'umi';

// 获取文章列表
export function getArticleList(page: number, pageSize=12, status='publish'){
    return request('/api/article', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}

// 获取推荐文章
export function getRecommend(articleId?: string){
    let query = '';
    articleId && (query = `?articleId=${articleId}`)
    return request('/api/article/recommend'+query)
}

// 获取文章评论
export function getArticleComment(id:string, page=1, pageSize=6){
    return request(`/api/comment/host/${id}?page=${page}&pageSize=6`)
}

// 获取文章详情
export function getArticleDetail(id: string){
    return request(`/api/article/${id}/views`, {
        method: 'POST'
    })
}