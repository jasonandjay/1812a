import HighLight from '@/components/HighLight';
import ImageView from '@/components/ImageView';
import { IRootState } from '@/types';
import { fomatTime } from '@/utils';
import React, { useEffect, useState } from 'react';
import { IRouteComponentProps, useDispatch, useSelector } from 'umi';

const ArticleDetail: React.FC<IRouteComponentProps<{ id: string }>> = props => {
  let id = props.match.params.id;
  const dispatch = useDispatch();
  const { articleDetail, articleComment, articleCommentCount } = useSelector((state: IRootState) => state.article)
  const [commentPage, setCommentPage] = useState(1);

  // 获取文章详情
  useEffect(() => {
    dispatch({
      type: 'article/getArticleDetail',
      payload: id
    })
  }, []);

  // 获取评论
  useEffect(() => {
    dispatch({
      type: 'article/getArticleComment',
      payload: {
        id,
        page: commentPage
      }
    })
  }, [commentPage]);

  let password = null,
    pay = null;
  if (articleDetail.needPassword){
    password = '';
  }
  if (articleDetail.totalAmount){
    pay = <button onClick={async ()=>{
      let result = await fetch('http://127.0.0.1:7001/pay', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({totalAmount: articleDetail.totalAmount, id: articleDetail.id})
      }).then(res=>res.json());
      if (result.url){
        window.location.href = result.url;
      }

    }}>立即支付：¥{articleDetail.totalAmount}</button>
  }
  return <div>
    <ImageView>
      <HighLight>
        {/* 渲染封面 */}
        {password}
        {pay}
        {articleDetail.cover && <img src={articleDetail.cover} alt="" />}
        <p>{articleDetail.title}</p>
        <p>发布于{fomatTime(articleDetail.publishAt!)} • 阅读量 {articleDetail.views}</p>
        {!password && <div dangerouslySetInnerHTML={{ __html: articleDetail.html! }}></div>}
        <p>发布于{fomatTime(articleDetail.publishAt!)}  | 版权信息：非商用-署名-自由转载</p>
      </HighLight>
    </ImageView>
  </div>
}

export default ArticleDetail;