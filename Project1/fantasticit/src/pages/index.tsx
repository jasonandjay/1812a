import { IArticleItem, IRootState } from '@/types';
import { useCallback, useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { NavLink, useDispatch, useHistory, useSelector } from 'umi';
import styles from './index.less';  // 启用css-module
import ArticleList from '@/components/ArticleList';
import React from 'react';
import IndexContext from '@/context';
import Count from '@/components/Count';
import useWinSize from '@/hooks/useWinSize';
import useGeolocation from '@/hooks/useGeolocation';
const classNames = require('classnames');

const WrapArticleList = React.memo(ArticleList);

export default function IndexPage() {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch()
  const history = useHistory();
  const { recommend, articleList, articleCount } = useSelector((state: IRootState) => state.article);
  const [count, setCount] = useState(0);

  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
    // setInterval(() => {
    //   setCount(count => count + 2);
    //   // setPage(page=>page+2);
    // }, 1000);
  }, [])

  // useLayoutEffect(() => {
  //   alert('count value is ' + count)
  // }, [count])

  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page]);

  function pullupLoader() {
    setPage(page => page + 1)
  }

  console.log('index page render');
  const size = useWinSize();
  const geoStatus = useGeolocation();

  return (
    <IndexContext.Provider value={{title: 'Index Page'}}>
      <div className={styles.wrap}>
        <Count></Count>
        <div>
          <p>窗口大小</p>
          <p>
            <span>宽： {size.width}</span>
            <span>高： {size.height}</span>
          </p>
        </div>
        <div>
          <p>当前位置信息</p>
          <p>
            <span>经度：{geoStatus.latitude}</span>
            <span>维度：{geoStatus.longitude}</span>
            <span>{JSON.stringify(geoStatus)}</span>
          </p>
        </div>
        <h1 className={classNames(styles.local, styles.title)}>Page index</h1>
        <div>{
          recommend.map(item => {
            return <li key={item.id}>
              <span>{item.title}</span>
            </li>
          })}</div>
        <section>
          <button>+</button>
          <span>{count}</span>
          <button>-</button>
        </section>
        <WrapArticleList
          page={page}
          articleList={articleList}
          articleCount={articleCount}
          object={useMemo(() => { page }, [page])}
          callback={useCallback(() => { }, [])}
        ></WrapArticleList>
      </div>
    </IndexContext.Provider>
  );
}
