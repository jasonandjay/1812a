import { IRootState } from '@/types';
import React, { useEffect } from 'react';
import { NavLink, setLocale, useDispatch, useIntl, useSelector } from 'umi';
import {Spin, Select} from 'antd';
import styles from './index.less';
import MyHeader from '@/components/MyHeader';

const Layouts: React.FC = (props)=>{
    const intl = useIntl();
    const dispatch = useDispatch();
    const {global, language} = useSelector((state:IRootState)=>{
        return {
            global: state.loading.global,
            language: state.language
        }  
    })

    useEffect(() => {
        setLocale(language.locale, false);
    }, [language.locale]);
    
    function changeLanguage(locale:string){
        dispatch({
            type: 'language/save',
            payload: {locale}
        })
    }

    const menus = [{
        title: 'menu.article',
        link: '/'
    }, {
        title: 'menu.archive',
        link: '/archive'
    }, {
        title: 'menu.knowledge',
        link: '/knowledge'
    }, {
        title: 'menu.message',
        link: '/message'
    }, {
        title: 'menu.about',
        link: '/about'
    }, ]
    return <div>
        {/* 全局loading */}
        {global && <div className={styles.loading}><Spin size="large"/></div>}
        {/* 头部区域 */}
        <MyHeader></MyHeader>
        {/* <header>
            <ul>{
                menus.map(item=>{
                    return <NavLink to={item.link} key={item.link}>
                        {intl.formatMessage({id: item.title})}
                    </NavLink>
                })
            }</ul>
            <Select value={language.locale} onChange={val=>changeLanguage(val)}>{
                language.locales.map(item=>{
                    return <Select.Option key={item.value} value={item.value}>{intl.formatMessage({id: item.label, defaultMessage: ''})}</Select.Option>
                })
            }</Select>
        </header> */}
        <main className={styles.wrap}>
            <div className="container">
                {props.children}
                <footer>底部</footer>
            </div>
        </main>
    </div>
}

export default Layouts;