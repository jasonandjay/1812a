import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import ReactDOM from 'react-dom';
import { IArticleItem } from '@/types';
import QRCode from 'qrcode'
import { genePoster } from '@/services/module/poster';

interface IProps {
    item: IArticleItem;
}
export const Share: React.FC<IProps> = ({ item }) => {
    const [qrSrc, setQrSrc] = useState('');
    const [downloadSrc, setDownloadSrc] = useState('');

    function cancelClick() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#share-dialog')!);
    }
    const url = `https://blog.wipi.tech/article/${item.id}`;
    useEffect(() => {
        if (Object.keys(item)) {
            QRCode.toDataURL(url)
                .then((url: string) => {
                    setQrSrc(url)
                })
                .catch(err => {
                    console.error(err)
                })
        }
    }, [item]);
    // 生成海报
    useEffect(() => {
        if (Object.keys(item).length && qrSrc) {
            genePoster({
                height: 861,
                html: document.querySelector('.ant-modal-content')?.innerHTML!,
                name: item.title,
                pageUrl: `/article/${item.id}`,
                width: 391
            }).then(res => {
                setDownloadSrc(res.data.url);
            })
        }
    }, [item, qrSrc]);
    return <Modal
        visible={true}
        title="分享海报"
        okText="下载"
        onOk={()=>window.location.href = downloadSrc}
        onCancel={cancelClick}
    >
        {item.cover && <img src={item.cover} />}
        <p>{item.title}</p>
        <p>{item.summary}</p>
        <div>
            <img src={qrSrc} alt="" />
            <div>
                <p>识别二维码查看文章</p>
                <p>原文分享自
                    <a href={url}>小楼又清风</a>
                </p>
            </div>
        </div>
    </Modal>
}

export default function share(item: IArticleItem) {
    let shareDialog = document.querySelector('#share-dialog');
    if (!shareDialog) {
        shareDialog = document.createElement('div');
        shareDialog.id = 'share-dialog';
        document.body.appendChild(shareDialog);
    }
    ReactDOM.render(<Share item={item} />, shareDialog);
}