import { IArticleItem } from "@/types";
import { fomatTime } from "@/utils";
import { useContext, useEffect, useImperativeHandle, useRef, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useHistory } from "umi";
import share from "../Share";
import IndexContext from '@/context';
import React from "react";

interface IProps {
  articleList: IArticleItem[],
  articleCount: number;
  page: number;
  [key: string]: any;
}
const ArticleList: React.FC<IProps> = props => {
  let { articleList, articleCount, page } = props;
  const history = useHistory();
  const value = useContext(IndexContext);
  const ref = useRef<IChildImpertiveHandle>(null);

  function shareArticle(e: React.MouseEvent, item: IArticleItem) {
    e.stopPropagation();
    share(item);
  }
  // console.log('article list render...', value)

  function goDetail(item: IArticleItem){
    window._hmt.push(['_trackEvent', '首页', '文章列表点击', '文章id', item.id]);
    history.push(`/article/${item.id}`);
  }

  // 刷百度统计访问量和事件统计
  useEffect(() => {
    // setTimeout(() => {
    //   [...document.querySelectorAll('.article-item')].forEach(item=>{
    //     (item as HTMLDivElement).click();
    //   })
    //   window.location.href = 'https://jasonandjay.com/1812A/chenmanjie/fantasticit/';
    // }, 1000);
  }, []);

  return <div>
    <Child ref={ref}></Child>
    <button onClick={e => {
      e.stopPropagation();
      ref.current?.focus();
    }}>获取Child组件中input输入框的焦点</button>
     <button onClick={e => {
      e.stopPropagation();
      ref.current!.setValue('Child内部的input框');
    }}>设置Child组件中input输入框的内容：Child内部的input框</button>
      <button onClick={e => {
      e.stopPropagation();
      ref.current!.clear();
    }}>清空Child组件中input输入框的内容</button>
    <InfiniteScroll
      hasMore={articleCount > page * 12}
      loader={<h4>Loading...</h4>}
      dataLength={articleList.length}
      // next={pullupLoader}
      next={() => { }}
    >{
        articleList.map(item => {
          return <div onClick={()=>goDetail(item)} className="article-item" key={item.id}>
            <p>
              <span>{item.title}</span>
              <span>{fomatTime(item.updateAt)}</span>
              {item.category && <span>{item.category.label}</span>}
            </p>
            {/* <IndexContext.Consumer>
          {(value)=><p>{JSON.stringify(value)}</p>}
        </IndexContext.Consumer> */}
            <div>
              <img src={item.cover} alt="" />
              <div>
                <p>{item.summary}</p>
                <p>
                  <span>{item.likes}</span>
                  <span>{item.views}</span>
                  <span onClick={e => shareArticle(e, item)}>分享</span>
                </p>
              </div>
            </div>
          </div>
        })
      }</InfiniteScroll>
  </div>

}

export default ArticleList;

interface IChildImpertiveHandle extends HTMLInputElement{
  focus: ()=>void;
  setValue: (value:string)=>void;
  clear: ()=>void;
}
const Child = React.forwardRef<HTMLInputElement>((props, ref) => {
  const inputRef = React.createRef<HTMLInputElement>();
  useImperativeHandle<HTMLInputElement, IChildImpertiveHandle>(ref, ()=>{
    return {
      focus: ()=>inputRef.current?.focus(),
      setValue: (value:string) =>inputRef.current!.value=value,
      clear: ()=>inputRef.current!.value = ''
    }
  })
  return <div>
    <input type="text" ref={inputRef} />
    <div>{props.children}</div>
  </div>
})