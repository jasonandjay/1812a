import React, { useEffect } from 'react';
import hljs from 'highlight.js';
import './index.less';
import { copyText } from '@/utils/copy';

const HighLight: React.FC = props=>{
    const dom = React.createRef<HTMLDivElement>();
    useEffect(() =>{
        let blocks = dom.current!.querySelectorAll('pre code');
        blocks.forEach(block=>{
            hljs.highlightElement(block as HTMLElement);
            // 添加复制按钮
            let copyBtn = document.createElement('button');
            copyBtn.textContent = '复制';
            copyBtn.className = 'copy-btn';
            copyBtn.onclick = function(){
                copyText(block.textContent!);
            }
            block.parentNode!.insertBefore(copyBtn, block);
        })
    }, [props.children]);
    
    return <div ref={dom} className="markdown">{props.children}</div>
}

export default HighLight;