import React, { useState } from 'react';
import { SketchPicker, ColorResult } from 'react-color';
import styles from './index.less';

const ColorPick: React.FC = () => {
    const [color, setColor] = useState('');

    function changeColor(color: ColorResult) {
        let compareColor = getComapreColor(color.hex);
        setColor(color.hex);
        document.body.style.setProperty('--bg-body', color.hex);
        document.body.style.setProperty('--main-text-color', compareColor);

    }
    function getComapreColor(hex: string){
        let colors:string[] = [];
        for (let i=0;i<3;i++){
            colors.push(`${hex[i*2+1]}${hex[i*2+2]}`)
        }
        return '#'+colors.map(item=>{
            return Number((255-parseInt(item, 16))%255).toString(16);
        }).join('');

    }
    return <div className={styles.wrap}>
        <SketchPicker
            color={color}
            onChangeComplete={changeColor}
        />
    </div>
}
export default ColorPick;