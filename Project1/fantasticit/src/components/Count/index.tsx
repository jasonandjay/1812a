import React, { useDebugValue, useImperativeHandle, useReducer, useRef } from 'react';

// function pick<T extends Object, R extends keyof T>(info: T, keys: R[]): T[R][]{
//     return keys.map(key=>info[key])
// }
const Count = ()=>{
    const div = useRef(null);
    console.log('ref...', div.current);

    const reducer = (state, action)=>{
        switch(action.type){
            case "+": {return {...state, num:state.num+1}};break;
            case "-": {return {...state, num:state.num-1}};break;
            default: return state;
        }
    }
    
    const initialState = {
        date: +new Date,
        num: 1000
    }
    const [state, dispatch] = useReducer(reducer, initialState, initialState=>({num: initialState.num*10, date: initialState.date}));

    const ref = useRef();

    return <div ref={div}>
        <button onClick={()=>dispatch({type: '+'})}>+</button>
        <span>{state.num}</span>
        <button onClick={()=>dispatch({type: '-'})}>-</button>
    </div>
}
export default Count;
