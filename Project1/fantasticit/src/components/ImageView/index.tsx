import React, { useEffect } from 'react';
import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.css';

const ImageView: React.FC = props=>{
    const dom = React.createRef<HTMLDivElement>();
    useEffect(() =>{
        let viewer = new Viewer(dom.current!);
        let ob = new MutationObserver(()=>{
            viewer.update();
        }); 
        ob.observe(dom.current!, {
            childList: true, 
            subtree: true
        });
        return ()=>{
            ob.disconnect();
            viewer.destroy();
        }
    }, [])

    return <div ref={dom}>{props.children}</div>
}

export default ImageView;