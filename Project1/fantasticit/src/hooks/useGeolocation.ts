import { useDebugValue, useEffect, useState } from 'react';

const useGeolocation = (options={}) => {
  const [state, setState] = useState({
    loading: true,
    accuracy: 0,
    altitude: 0,
    altitudeAccuracy: 0,
    heading: 0,
    latitude: 0,
    longitude: 0,
    speed: 0,
    timestamp: Date.now(),
  });
  let mounted = true;
  let watchId: any;

  const onEvent = (event: GeolocationPosition) => {
    if (mounted) {
      setState({
        loading: false,
        accuracy: event.coords.accuracy,
        altitude: event.coords.altitude!,
        altitudeAccuracy: event.coords.altitudeAccuracy!,
        heading: event.coords.heading!,
        latitude: event.coords.latitude,
        longitude: event.coords.longitude,
        speed: event.coords.speed!,
        timestamp: event.timestamp,
      });
    }
  };
  const onEventError = (error: GeolocationPositionError) =>
    mounted && setState(oldState => ({ ...oldState, loading: false, error }));

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(onEvent, onEventError, options);
    watchId = navigator.geolocation.watchPosition(onEvent, onEventError, options);

    return () => {
      mounted = false;
      navigator.geolocation.clearWatch(watchId);
    };
  }, []);
  useDebugValue(state);

  return state;
};
export default useGeolocation;