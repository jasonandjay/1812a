import { createApp } from 'vue'
import App from './App.vue'

// 使用createApp创建vue使用
createApp(App).mount('#app')
