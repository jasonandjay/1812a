import Dep from './dep.js';

class Watch{
    constructor(vm, exp, cb){
        this.vm = vm;
        this.exp = exp;
        this.cb = cb;
        this.get();
    }
    get(){
        Dep.target = this;
        this.getVal();
        Dep.target = null;
    }
    getVal(){
        // exp，如film.thumb
        let value = this.exp.split('.').reduce((value, key)=>{
            return value[key]
        }, this.vm);
        return typeof value === 'function' ? value.call(this.vm): value;
    }
    update(){
        this.cb(this.getVal());
    }
}

export default Watch;