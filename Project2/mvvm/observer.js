import Dep from './dep.js';

class Observer{
    constructor(data, vm){
        this.data = data || {};
        this.vm = vm;
        this.proxy(data);
        this.dep = new Dep();
    }
    proxy(data){
        for (let key in data){
            if (typeof data[key] === 'object' && data[key] !== null){
                this.proxy(data[key]);
            }
            this.defineReactive(data, key, data[key]);
        }
    }
    defineReactive(data, key, value){
        const me = this;
        Object.defineProperty(data, key, {
            get(){
                if (Dep.target){
                    me.dep.addSub(Dep.target);
                }
                return value;
            },
            set(newVal){
                value = newVal;
                me.dep.notify();
            }
        })
    }
}

export default Observer;