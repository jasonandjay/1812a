import Observer from './observer.js';
import Compile from './compile.js';

class MVVM{
    constructor(options){
        this.$options = options;
        this.$data = options.data;
        // 代理data数据内部属性到this
        this._proxy(this.$data);
        // 代理computed数据内部属性到this
        this._proxy(options.computed);
        // 代理methods数据内部属性到this
        this._proxy(options.methods);

        // 劫持属性
        new Observer(this.$data, this);

        // 编译模版
        new Compile(options.el, this);
    }
    _proxy(data){
        for (let key in data){
            Object.defineProperty(this, key, {
                get(){
                    return data[key]
                },
                set(newVal){
                    data[key] = newVal;
                }
            })
        }
    }
}

export default MVVM;