import Watch from './watch.js';

class Compile{
    constructor(el, $vm){
        this.$vm = $vm;
        // 获取要挂载的节点
        this.$el = el?document.querySelector(el): document.body;
        // 把节点转化成dom碎片编译
        this.$fragment = this.node2Fragment(this.$el);
        // 编辑模版
        this.compile(this.$fragment);
        // 把dom碎片添加到挂载节点后面
        this.$el.appendChild(this.$fragment);
    }  
    // dom节点转化成fragment
    node2Fragment($el){
        let $fragment = document.createDocumentFragment();
        if ($el.firstElementChild){
            $fragment.appendChild($el.firstElementChild);
        }
        return $fragment;
    } 
    // 编译模版
    compile(el){
        let childNodes = el.childNodes;
        [...childNodes].forEach(node=>{
            if (node.nodeType === 1){
                this.compileElement(node);
            }else if (node.nodeType === 3){
                const textReg = /([^\{}]+)*\{\{(.*)\}\}([^\{}]+)*/;
                if (textReg.test(node.textContent)){
                    compileUtil.compileText(node, this.$vm, RegExp.$1, RegExp.$2, RegExp.$3)
                }
            }
            if (node.childNodes && node.childNodes.length){
                this.compile(node);
            }
        })
    }
    // 处理元素节点
    compileElement(node){
        let attributes = node.attributes;
        [...attributes].forEach(attr=>{
            if (this.isDirective(attr.name)){
                if (this.isEventDirective(attr.name)){
                    compileUtil.compileEventHandle(node, this.$vm, attr.name, attr.value);
                }else{
                    let dir, handleMethod;
                    if (attr.name.includes(':')){
                        dir = attr.name.split(':')[1];
                    }else{
                        dir = attr.name.slice(2);
                    }
                    handleMethod = 'compile'+dir[0].toUpperCase()+dir.slice(1);
                    console.log('handleMethod...', handleMethod);
                    compileUtil[handleMethod] && compileUtil[handleMethod](node, this.$vm, attr.value);
                }
                node.removeAttribute(attr.name);
            }
        })
    }
    // 属性是否是指令
    isDirective(name){
        return name.includes('v-') || name.includes(':') || name.includes('@');
    }
    // 属性是否是事件
    isEventDirective(name){
        return name.includes('v-on') || name.includes('@');
    }
}

// 编辑工具
const compileUtil = {
    bindReactive(node, vm, exp, type, options){
        let val = '';
        if (type === 'updateFor'){
            const forExp = /\(([^,]+)(, ([^,]+))?\) in ([\w]+)/;
            if (forExp.test(exp)){
                if (RegExp.$2.includes(',') !== -1){
                    options = {
                        item: RegExp.$1,
                        key: RegExp.$3,
                        val: RegExp.$4
                    };
                }else{
                    options = {
                        item: RegExp.$1,
                        key: RegExp.$2,
                        val: RegExp.$3
                    };
                }
                val = this._getVMVal(vm, options.val);;
                exp = options.val;
            }else{
                console.error('v-for格式错误...', exp);
            }
        }else{
            val = this._getVMVal(vm, exp);
        }
        update[type] && update[type](node, val, options);
        // 对使用到的exp添加监听
        new Watch(vm, exp, (newValue) =>{
            update[type](node, newValue, options);
        })
    },
    compileSrc(node, vm, exp){
        this.bindReactive(node, vm, exp, 'updateAttribute', {attr: 'src'});
    },
    compileFor(node, vm, exp){
        this.bindReactive(node, vm, exp, 'updateFor');
    },
    compileIf(node, vm, exp){
        this.bindReactive(node, vm, exp, 'updateIf');
    },
    compileText(node, vm, preText, exp, nextText){
        this.bindReactive(node, vm, exp, 'updateText', {preText, nextText});
    },
    compileHtml(node, vm, exp){
        this.bindReactive(node, vm, exp, 'updateHtml');

    },
    compileModel(node, vm, exp){
        if (node.type === 'checkbox' || node.type === 'radio'){
            this.bindReactive(node, vm, exp, 'updateCheck');
            node.addEventListener('change', (e)=>{
                this._setVMVal(vm, exp, e.target.checked)
            })
        }else{
            this.bindReactive(node, vm, exp, 'updateValue');
            node.addEventListener('change', (e)=>{
                this._setVMVal(vm, exp, e.target.value)
            })
        }
    },
    compileEventHandle(node, vm, dir, exp){
        if (dir.includes('@') !== -1){
            dir = dir.slice(1);
        }else{
            dir = dir.split(':')[1];
        }
        let fn = vm.$options.methods && vm.$options.methods[exp];
        if (dir && fn){
            node.addEventListener(dir, fn.bind(vm));
        }
    },
    _getVMVal(vm, exp){
        let keys = exp.split('.');
        let val = vm;
        for (let i = 0; i < keys.length; i++){
            val = val[keys[i]];
        }
        return typeof val==='function' ?val.call(vm): val;
    },
    _setVMVal(vm, exp, newValue){
        let keys = exp.split('.');
        let val = vm;
        for (let i = 0; i < keys.length; i++){
            if (i === keys.length - 1){
                val[keys[i]] = newValue;
            }else{
                val = val[keys[i]];
            }
        }
        return val;
    }
}

// 更新工具
const update = {
    parentNode: null,
    updateText(node, val, options){
        node.textContent = `${options.preText+val+options.nextText}`;
    },
    updateHtml(node, val){
        node.innerHTML = val;
    },
    updateCheck(node, val){
        node.check = val;
    },
    updateValue(node, val){
        node.value = val;
    },
    updateAttribute(node, val, attr){
        node.setAttribute(attr.attr, val);
    },
    updateIf(node, val){
        console.log('val...', val);
        let parentNode = null;
        if (node.parentNode){
            parentNode = this.parentNode = node.parentNode;
        }else{
            parentNode = this.parentNode;
        }
        if (!val){
            if ( parentNode === node.parentNode ){
                parentNode.removeChild(node);
            }
        }else{
            parentNode.appendChild(node);
        }
    },
    updateFor(node, val, options){
      console.log('arguments...', arguments);
      const nodeParentKey = `${options.value}_parent`,
            nodeTempKey = `${options.value}_temp`;
        let parentNode = null, 
            tempNode = null;
        if (node.parentNode){
            this[nodeParentKey] = parentNode = node.parentNode;
            this[nodeTempKey] = tempNode = node.cloneNode(true);
        }else{
            parentNode = this[nodeParentKey];
            tempNode = this[nodeTempKey];
        }

        let fragment = document.createDocumentFragment();
        // 遍历循环列表
        for (let key in val){
            let child = tempNode.cloneNode(true);
            // 替换属性index
            [...child.attributes].forEach(item=>{
                child.setAttribute(item.name, item.value.replace(`${options.key}`, key))
            })
            // 替换属性item
            child.innerText = child.innerText.replace(`{{${options.item}}}`, val[key]);
            fragment.appendChild(child);
        }   
        // 清空以前的节点
        [...parentNode.childNodes].forEach(child=>{
            child.parentNode.removeChild(child);
        })
        // 增加新的节点
        parentNode.appendChild(fragment);
    }
}


export default Compile;