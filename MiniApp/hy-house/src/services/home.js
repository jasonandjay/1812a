import request from '@/utils/request';

// 首页轮播图
export function getBanner(){
    return request.get('/home/swiper')
}

// 获取房屋分类
export function getType(){
    return request.get('http://www.sqfcw.com/wapi/index/infoData.html?type=xinfang&setting=25');
}