import request from '@/utils/request';

// 大牌特惠页面配置
export function getShopPage() {
    return request.post('/msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&m=zofui_sales&sign=48ec8d461e61d4415da38fc52a5693fa', {
        zfid: 0,
        isnew: 1,
        isposter: 0,
        shopid: 0,
        op: 'info'
    }, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}