import { userLogin } from "@/services";

const state = {
    userInfo: {},
    isLogin: false
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async userLogin({commit}, payload){
        let res = await userLogin(payload);
        console.log('res...', res);
        if (res.code === 0){
            commit('update', {
                userInfo: res.data,
                isLogin: true
            });
            // 存储登陆态到本地存储
            wx.setStorageSync('openid', res.data.openid);
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}