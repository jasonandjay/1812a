import Vue from 'vue'
import App from './App'
// 引入vuex
import store from '@/store'
import VueCompositionAPI from '@vue/composition-api'
// 引入过滤器
import * as filters from '@/filters';
for (let filter in filters) {
  Vue.filter(filter, filters[filter]);
}
Vue.use(VueCompositionAPI)


Vue.config.productionTip = false;
// Vue.prototype.$store = store;

App.mpType = 'app'

const app = new Vue({
  store,
  ...App,
})
app.$mount()
