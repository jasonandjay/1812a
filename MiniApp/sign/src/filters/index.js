export function getDistance(distance) {
    return distance.toFixed(2);
}

export function formatTime(timestamp){
    let date = new Date(Number(timestamp));
    let year = date.getFullYear(),
        month = String(date.getMonth()).padStart(2, '0'),
        day = String(date.getDate()).padStart(2, '0'),
        hour = String(date.getHours()).padStart(2, '0'),
        minute = String(date.getMinutes()).padStart(2, '0'),
        sec = String(date.getSeconds()).padStart(2, '0');
    return `${year}-${month}-${day} ${hour}:${minute}:${sec}`;
}