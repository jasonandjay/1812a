## 小程序部分知识点

### 小程序结构
- wxml
- wxss
- js
- json

### 小程序路由
#### 路由跳转
- Api
  - push: wx.navigateTo({url: ''})
  - replace: wx.redirectTo({url: ''})
  - 回退：wx.navigateBack()
  - 切换tabbar: wx.switchTab({url:''})
  - 重新加载：wx.reLaunch({url:''})
- Navigator组件

#### 小程序的生命周期
- 应用的生命周期
  - onLaunch
- 页面的生命周期
  - onLoad
  - onReady
  - onShow
  - onHide
  - onUnLoad
  - onShareAppMessage   分享
  - onPullDownRefresh   下拉刷新
  - onReachBottom       上拉加载

#### 小程序样式
- 响应式单位：rpx
```css
在屏幕宽度750
100rpx = 100px;
1rem = 100px;
在屏幕宽度375
100rpx = 50px;
1rem = 50px;

得出结论：
1rem = 100rpx;
```